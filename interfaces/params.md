# Interface Params

## Description

## API 


```
components:
  schemas:
    Params:
      type: array
      items:
        $ref: "#/components/schemas/Param"
    Param:
      type: object
      properties:
        id:
            type: string
        value:
            type: string
```