# Interface Access

## Description

## API 


```
components:
  schemas:
    Interval:
      type: object
      properties:
        begin:
            type: string
            format: date-time
        end:
            type: string
            format: date-time
    Position:
      type: object
      properties:
        latitude:
            type: number
        longitude:
            type: number
    PositionDate:
      type: object
      properties:
        position:
          $ref: "#/components/schemas/Position"
        date:
            type: string
            format: date-time
    PositionDates:
      type: array
      items:
        $ref: "#/components/schemas/PositionDate"
    Access:
      type: object
      properties:
        interval:
          $ref: "#/components/schemas/Interval"
    Accesses:
      type: array
      items:
        $ref: "#/components/schemas/Access"
```
