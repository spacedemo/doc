# Interface UserRequest

## Description

## API 


```
components:
  schemas:
    UserRequestCreation:
      type: object
      properties:
        email_user:
          type: string
        positionDatee:
          $ref: "#/components/schemas/PositionDate"
    UserRequests:
      type: array
      items:
        $ref: "#/components/schemas/UserRequest"
    UserRequest:
      type: object
      properties:
        id :
          type: string
        position:
          $ref: "#/components/schemas/Position"
        creation_date:
          type: string
          format: datetime
        email_user:
          type: string
```
