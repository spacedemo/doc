# Interface Acquisition

## Description

## API 


```
components:
  schemas:
    Acquisitions:
      type: array
      items:
        $ref: "#/components/schemas/Acquisition"
    Acquisition:
      type: object
      properties:
        date:
            type: string
            format: datetime
```
