# Space Demo
Spécification préliminaire

## Présentation du projet

### Présentation du besoin

Ce projet est issu d'un besoin simple : 

Comment faire comprendre au monde quel est notre travail ! Comment un jour nous pourrions faire comprendre à nos familles, enfants, amis, connaissance la complexité et la diversité de nos activités.

Nous, ce sont tous les travailleurs du monde du logiciel spatial.

Qui dans notre monde n'a pas déjà passé des heures à expliquer qu'un satellite, ce n'est rien d'autre qu'une boite de conserve piloté a distance. Et que justement ça ne ce pilote pas comme une voiture télécommandé !

Confronté à cette problématique a émergé le désir de produire une plateforme de démonstration permettant de rendre compréhensible et accessible l'explication du fonctionnement d'un satellite de prises de vue terrestre aux non initiés.

Une telle plateforme peut servir de multiple façon :

- Présenter les métiers du spatial à des enfants ou des étudiants pour essayer de faire naitre des vocations (et oui Astraunaute n'est pas le seul moyen d'atteindre les étoiles).
- Présenter les métiers et technologies utilisées par une société du monde du spatial en salon de recrutement pour être plus attirante que la dernière startup du coin avec ses jeux mobiles.
- Permettre de tester de nouvelles approches sur une plateforme physique simplifié des différentes problématiques algorythmiques de nos produits.
- Tout simplement faire une super décoration en salle de pause pour en jeter devant les clients / passant circulant dans le couloir.


### Présentation succinte du projet

L'idée du projet est de produire une maquette composée de :

- Un globe terrestre rotatif déssiné ou non, en relief ou non, en fonction de ce qui est trouvable.
- Un satellite imprimé en 3D qui gravite autour (non par magie, mais grace à un anneau sur rail motorisé).

Ce satellite serait composé d'une caméra,  d'indicateur lumineux d'état et d'un laser pour visualiser la trace au sol.

- Une station Sol gérant tout le reste. Cette station sol permettrait de piloter l'ensemble du système. Elle sera composée des moyens informatiques.
- Un QR code permetant à un utilisateur d'accéder à un site de saisie de la demande.

Le principe est donc d'avoir un globe terrestre rotatif ainsi qu'un anneau motorisé autour de ce globe. L'association de ces deux mouvements permettant de simuler un mouvement d'orbite inclinée circulaire basse.

Le satellite sera relié par une liaison sans fil à la station sol.

La station sol permettra de commander la motorisation du système ainsi que de permettre l'affichage graphique des éléments nécessaires à la vulgarisation de la démonstration.
Une partie des moyens logiciels (planification, centre de contrôle) pourront être hébergés sur la station sol.

Le QR code permet à l'utilisateur d'accéder à une page publique de saisie d'une demande sur un planisphère par exemple. 

Une fois la saisi faites et planifiées, elle sera physiquement effectuée par la caméra embarquée dans le satellite imprimé en 3D et envoyer à la station sol.
Une fois reçu, elle sera renvoyée par mail à l'utilisateur ayant fait la demande.

## La partie mécanique

### Globe terrestre rotatif

Motorisé par un moteur pas à pas NEMA14

### Cercle de soutient satellite

Motorisé par un moteur pas à pas NEMA14

### Satellite

Composé d'un ESP32-CAM + 3 Led + 1 laser + 1 capteur de couleur + 1 LED RGB 5050

## La partie Hardware

### Centre de Controle

Composé d'un Raspeberry PI

### Contrôle Orbite

Le système de contrôle d'orbit sert au pilotage de la rotation du globe terrestre et du disque de soutient du satellite.

Le système est composé de :

- 1 [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3)
- 2 controleur de moteur pas à pas [EasyDrivers](https://www.schmalzhaus.com/EasyDriver/Examples/EasyDriverExamples.html)
- 1 switch d'indication de repère de rotation du globe terrestre
- 1 switch d'indication de repère de rotation du disue de soutient du satellite

Le contrôle d'orbit est assuré par le composant logiciel [DemoControl](#Composant : DemoControl)

Le système de controle d'orbite utilise un point de référence pour chaque axe de rotation grace au switch.
A chaque démarrage du système, le controleur d'orbit calibre la position du satellite en utilisant les points de références et ratrappe la position attendue en utilisant un asservissement de position permettnt de ratrapper la position.

La liaison avec le Centre de Contrôle esst assuré par une liaison USB - Lien Série 115200

## Les types de bases :

- Dates : format ISO8601 : YYYY-MM-DDTHH:mm:SS.000Z
- latitude, longitude : radians float 

## Les composants Logiciels

```plantuml
actor User
boundary Satellite
boundary Globe
boundary DisqueOrbite

component Guidance
component UserRequest
component Planning
component ControlCenter
component Catalog
component DemoControl

User --> UserRequest
UserRequest --> Planning
Planning --> ControlCenter
ControlCenter ..> Satellite
Satellite ..> Catalog
Catalog --> User

Planning --> Guidance
ControlCenter --> Guidance
DemoControl --> Guidance

DemoControl --> Globe
DemoControl --> DisqueOrbite
```

### [Composant : Guidance](./composants/guidance.md)

Production : [guidance.spacedemo.escande.ovh](http://guidance.spacedemo.escande.ovh/swagger-ui.html)

Responsable : Philippe Pavero

Membres : 


### [Composant : UserRequest-UI](./composants/userrequest.md) 

Responsable : Laurent Cocault 

Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.


### [Composant : UserRequest](./composants/userrequest-ui.md) 

Responsable : Mathieu

Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

### [Composant : Planning](./composants/planning.md) 

Production : [planner.spacedemo.escande.ovh](http://planner.spacedemo.escande.ovh/swagger-ui.html)

Responsable : Philippe
Membres : 
  - Matthieu

Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

### [Composant : ControlCenter](./composants/controlcenter.md)

Production : [control-center.spacedemo.escande.ovh](http://control-center.spacedemo.escande.ovh/swagger-ui.html)

Responsable : Guillaume
Membres : 
  - Matthieu

Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

### [Composant : Catalog](./composants/catalog.md)
 
Responsable : Xavier

Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

### [Composant : DemoControl](./composants/democontrol.md)

Production : [demo-control-server.spacedemo.escande.ovh](http://demo-control-server.spacedemo.escande.ovh/swagger-ui.html)

Responsable : Guillaume

Le composant doit fournir poposer les services suivants :

- Service de synchronisation temporelle : Permet de synchroniser temporellement le controle de position du satellite avec le centre de contrôle
- Service de positionnement du satellite : Ce service prends en entrée la position attendue du satellite à un timestamp donné et positionne le satellite en fonction.

Le composant est composé de 2 PID pour contrôle de position.
La boucle de retour de position est fait en utilisant le comptage de déplacement des moteurs Pas à Pas.

### [Composant : Bord Software](./composants/bord.md)
 
Responsable : William

Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.

### [Composant : Supervisor](./composants/supervisor.md)

Responsable : Thomas
Membres : 
  - Matthieu

## Les Interfaces

### [UserRequest](./interfaces/userrequest.md)

L'interface UserRequest est émise par l'interface web accessible par QRCode permettant à l'utilisateur de saisir une demande de prise de vue.

### [Plan](./interfaces/plan.md)

### [Acquisition](./interfaces/acquisition.md)

### [Acces](./interfaces/access.md)

### [Param](./interfaces/params.md)



