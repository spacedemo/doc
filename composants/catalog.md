# Catalog

## Description

## API

```
openapi: 3.0.0
info:
  title: Catalog component
  version: 2.0.0
paths:
  /image/{id}
    post:
      parameters:
      - name: "id"
        in: path
        description: "id the picture"
        required: true
        schema:
          type: string
      requestBody:
        content:
          image/png:
            schema:
              type: string
              format: binary

```


## Conception
