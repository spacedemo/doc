# User Request

## Description

## API

```
openapi: 3.0.0
info:
  title: User Request component
  version: 1.0.0
paths:
    "/user_request/{userRequestId}":
        get:
            tags:
                - user-request-api-controller
            description: "Renvoie la User Request correspondant à un identifiant donné"
            parameters:
                - name: userRequestId
                  in: path
                  description: Id of the user request
                  required: true
                  schema:
                      type: string
            responses:
                "200":
                    description: successful operation
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/UserRequest"
                "400":
                    description: Invalid ID supplied
                "404":
                    description: UserRequest not found
        delete:
            tags:
                - user-request-api-controller
            description: "Supprime la User Request correspondant à un identifiant donné"
            parameters:
                - name: userRequestId
                  in: path
                  description: id of user request
                  required: true
                  schema:
                      type: string
            responses:
                "200":
                    description: successful operation
                "400":
                    description: Invalid ID supplied
                "404":
                    description: UserRequest not found
    "/user_request/send_image":
        description: "Envoie par mail l'image de la User Request correspondant à un identifiant donné"
        post:
            tags:
                - user-request-api-controller
            requestBody:
                content:
                    multipart/form-data:
                        schema:
                            type: object
                            properties:
                                userRequestId:
                                    type: string
                                image:
                                    type: string
                                    format: binary
            responses:
                "200":
                    description: successful operation
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/UserRequest"
                "400":
                    description: Invalid ID supplied
                "404":
                    description: UserRequest not found
    "/user_request/":
        description: "Créé une User Request pour une position datée"
        post:
            tags:
                - user-request-api-controller
            requestBody:
                content:
                    application/json:
                        schema:
                          $ref: "#/components/schemas/UserRequestCreation"
            responses:
                "200":
                    description: successful operation
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/UserRequest"
        get:
            tags:
                - user-request-api-controller
            description: "Renvoie les User Request correspondant aux dates données"
            parameters:
                - name: begin
                  in: query
                  description: beginning date of user request list
                  required: true
                  schema:
                      type: string
                      format: date-time
                - name: end
                  in: query
                  description: ending date of user request list
                  required: true
                  schema:
                      type: string
                      format: date-time
            responses:
                "200":
                    description: successful operation
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/UserRequests"
                "400":
                    description: Invalid ID supplied
                "404":
                    description: UserRequest not found

```

Interfaces : 

- [UserRequest](../../interfaces/userrequest.md)
## Conception



## Autre
- v2

methods :

objects :

UserRequest
  - Polygon zone  (-> traitement d'image mozaique)
  - String email_user
  - Instant date_max
  - Instant creation_date
  - Condition éclairement
