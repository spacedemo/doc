# Guidance

## Description

## API

```
openapi: 3.0.0
info:
  title: Guidance component
  version: 1.0.0
paths:
  /accesses:
    get:
      summary: "get access from temporal access and location"
      parameters:
      - name: "positions"
        in: query
        description: "positions"
        required: true
        schema:
          $ref: "#/components/schemas/Positions"
      - name: "interval"
        in: query
        description: "time interval "
        required: true
        schema:
          $ref: "#/components/schemas/Interval"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Accesses"
  /trace/:
    get:
      summary: "Get Sat trace from time interval and step"
      parameters:
      - name: "interval"
        in: query
        description: "time interval "
        required: true
        schema:
          $ref: "#/components/schemas/Interval"
      - name: "step_ms"
        in: query
        description: "step of positions in ms"
        required: true
        schema:
          type: integer
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
               $ref: "#/components/schemas/PositionDates"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Param not found"
  /position/{date}:
    get:
      summary: "Get Sat position from timestamp"
      parameters:
      - name: "date"
        in: path
        description: "timestamp of position"
        required: true
        schema:
          type: string
          format: date-time
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
               $ref: "#/components/schemas/Position"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Param not found"
```

Interfaces : 

- [Access](../../interfaces/access.md)

## Conception
