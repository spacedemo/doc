# Control Center

## Description

## API

```
openapi: 3.0.0
info:
  title: Control Center component
  version: 2.0.0
paths:
  /param:
    get:
      summary: "get all params"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Params"
  /param/{paramId}:
    get:
      summary: "Find pet param ID"
      parameters:
      - name: "paramId"
        in: path
        description: "ID of Param to return"
        required: true
        schema:
          type: string
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Param"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Param not found"

```

Interfaces : 

- [Param](../../interfaces/params.md)

## Conception

Liste des Params :

- Delais entre acquisition
- Paramètres d'orbites + altitude
- Dimention de la fauché au sol (taille de la prise de vue) en km (-> avec altitude, a convertir en angle pour le planning. A voir si possible de spécifier en pixel ):
  - length_along_track (km)
  - length_accross_track (km)
