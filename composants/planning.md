# Planning

## Description

## API

```
openapi: 3.0.0
info:
  title: Planning component
  version: 1.0.0
paths:

  /plan:
    post:
      summary: "start compute plan (get all request, compute plan, store it in BDD)"
      responses:
        200:
          description: "successful operation"
    get:
      summary: "get plan"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Plan"
```

Interfaces : 

- [Acquisition](../../interfaces/acquisition.md)
- [Plan](../../interfaces/plan.md)
- [UserRequest](../../interfaces/userrequest.md)
- [Access](../../interfaces/access.md)

## Conception



## Autre
- v2

methods :

objects :

UserRequest
  - Polygon zone  (-> traitement d'image mozaique)
  - String email_user
  - Instant date_max
  - Instant creation_date
  - Condition éclairement
