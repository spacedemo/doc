# Space Demo

## Documentation :

### [Spécification Préliminaire](./specification-preliminaire.md)



## Compte-Rendu réunion

### 16/01/2020 - Réunion de lancement

#### Présent :
- Laurent
- Philippe
- Guillaume

#### Sujets :

- Présentation des premiers éléments matériel regroupé :
  - Globe de 30cm de diamètre (tout pourris, mais ca suffira pour demarrer)
  - Présentation de l'ESP32 qui service de plateforme satellite
  - Présentation du système de demo control. Il s'agit de 2 moteurs pas-a-pas + arduino pour controler la maquette phisique.

- Présentation du projet et des avancées actuelle
  - Présentation de la spec préliminaire

- Définition des responsabilité composants:
  - Philippe veux s'occuper du [Guidance](./composants/guidance.md) et du [Planning](./composants/planning.md)
  - Laurent s'occupe de [UserRequest](./composants/userrequest.md) et du [Catalog](./composants/catalog.md)
  - Guillaume s'occupe du [DemoControl](./composants/democontrol.md)

- Initialisation des API V1 pour certains composants
  - [Guidance](./composants/guidance.md)
  - [Planning](./composants/planning.md)
  - [UserRequest](./composants/userrequest.md)

#### Actions :

- @Guillaume : mettre au propre les Interfaces et API au format swagger
- @Laurent : Reprendre le projet Nemeo pour initialiser un frontal [UserRequest](./composants/userrequest.md)
- @Philippe : Prendre en main la techno java springboot pour [Guidance](./composants/guidance.md) et [Planning](./composants/planning.md)
- @All : mettre à jour la [Spécification Préliminaire](./specification-preliminaire.md) et les doc de composants.


### 30/01/2020 - Avancement 1

#### Présent :
- Mathieu
- Thomas
- Xavier
- Guillaume

#### Sujets :

- Proposition : Modification des API seulement en réunion.
  Souplesse sur de l'ajout d'API (tant que ca reste retro compatible).
  Chacun fait comme il le souhaite sur son composant. Mais la doc (branche master) n'est mise a jour (ajout ou update ou delete) que en scéance.
- Nouvelle affectation de responsabilité des composants
- Définition du composant [Supervisor](./composants/supervisor.md)
- Définition de l'API du [Catalog](./composants/catalog.md)


#### Actions :


- @Philippe :
  - porter le travail fait sur le [Guidance](./composants/guidance.md) dans le composant, finaliser les WS actuels
  - Implémenter un premier calcul de plan (en échangeant avec Matthieu) simpliste au possible.
- @Matthieu : 
  - Créer un nouveau composant sur le Gitlab [UserRequest](./composants/userrequest.md). 
Le mieux est de copier un projet déjà existant ( je peux l’initialiser si tu veux
  - Faire la gestion de la réception d’une image avec l’envoie de mail – interfaçage avec GCE envoie de mail
- @Xavier :
  - Implémenter les WS de catalog.
  - Regarder comment initialiser le composant UserRequest UI
- @Thomas :
  - Débroussailler le sujet du [Supervisor](./composants/supervisor.md) et voir comment on pourrait l’implémenter.
- @Guillaume :
  - Rédiger ce compte rendu ^^
  - Avancer la démo physique (modèle 3D de la démo)
  - Initialiser le [logiciel bord](./composants/bord.md) 

